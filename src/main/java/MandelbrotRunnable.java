import java.awt.image.BufferedImage;

public class MandelbrotRunnable implements Runnable {
    private final int number;
    private final int xStart, xEnd;
    private final int yStart, yEnd;
    private final int maxIterations;
    private final int height;
    private final int width;

    private final ColorsUtil colorsUtil;
    private final BufferedImage image;

    public MandelbrotRunnable(int xStart, int xEnd, int yStart, int yEnd, int height, int width, int maxIterations, ColorsUtil util, BufferedImage image, int number) {
        this.number = number;
        this.xStart = xStart;
        this.yStart = yStart;
        this.xEnd = xEnd;
        this.yEnd = yEnd;
        this.maxIterations = maxIterations;
        this.height = height;
        this.width = width;

        this.image = image;
        colorsUtil = util;
    }

    @Override
    public void run() {
        for (int x = xStart; x < xEnd; x++) {
            for (int y = yStart; y < yEnd; y++) {
                double xCoord = x / (double) width * 4.0;
                double yCoord = y / (double) height * 4.0;
                xCoord -= 2;
                yCoord = 2 - yCoord;

                double xWork = 0, yWork = 0, x2 = 0, y2 = 0;
                int iteration = 0;
                while (iteration <= maxIterations && x2 + y2 <= 4) {
                    yWork = 2 * xWork * yWork + yCoord;
                    xWork = x2 - y2 + xCoord;
                    x2 = xWork * xWork;
                    y2 = yWork * yWork;
                    iteration++;
                }
                
                image.setRGB(x, y, colorsUtil.getRGBColor(iteration));
            }
        }
    }
}

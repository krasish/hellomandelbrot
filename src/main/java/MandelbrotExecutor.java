import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MandelbrotExecutor {
    private static final int MAX_ITERATIONS = 1000;

    private final int width;
    private final int height;
    private final int threadCount;
    private final int granularity;
    private final boolean outputImage;
    private final String outputImagePath;

    private final ColorsUtil util;
    private final BufferedImage image;


    public MandelbrotExecutor(int width, int height, int threads, int granularity, boolean outputImage, String outputImageName) {
        if (height < threads) {
            throw new IllegalArgumentException(String.format("no work for %d threads in %dx%d image", threads, width, height));
        }

        this.width = width;
        this.height = height;
        this.threadCount = threads;
        this.granularity = granularity;
        this.outputImage = outputImage;
        this.outputImagePath = outputImageName;

        this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        this.util = new ColorsUtil(MAX_ITERATIONS);
    }

    public void execute() {
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

        int tasksCount = granularity * threadCount;
        int factor = height / tasksCount;

        System.out.printf("Starting Mandelbrot generation using %d threads...\n", threadCount);
        long startTime = System.currentTimeMillis(), endTime;
        for (int i = 0; i <= tasksCount; i++) {
            Runnable currentRunnable;
            int currentYStart = i * factor;
            int currentYEnd = ((i + 1) * factor);
            currentRunnable = new MandelbrotRunnable(0, width, currentYStart, currentYEnd, height, width, MAX_ITERATIONS, util, image, i);

            executorService.submit(currentRunnable);
        }
        executorService.shutdown();

        try {
            if (executorService.awaitTermination(1, TimeUnit.MINUTES)) {
                System.out.println("Mandelbrot generated!\n");
                endTime = System.currentTimeMillis();
            } else {
                System.out.println("Operation timed out");
                return;
            }

        } catch (InterruptedException e) {
            System.out.println("A thread got interrupted");
            return;
        }

        long time = endTime - startTime;
        System.out.println("Execution took: " + time + " ms");

        if (outputImage) {
            saveImage(outputImagePath);
        }
    }

    private void saveImage(String path) {
        try {
            File output = new File(path);
            ImageIO.write(this.image, "png", output);
        } catch (IOException e) {
            System.err.println("Could not save image.");
        }
    }
}
